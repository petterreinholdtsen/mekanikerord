SOURCE = pdf.xsl
SOURCE_NB = $(SOURCE) book.xml bookinfo-nb.xml glossary-nb.xml
SOURCE_SE = $(SOURCE) book-se.xml bookinfo-se.xml glossary-se.xml
SOURCE_EN = $(SOURCE) book-en.xml bookinfo-en.xml glossary-en.xml

GENERATED = \
  mekaniker-ordbok-nb.pdf mekaniker-ordbok-nb.epub mekaniker-ordbok-nb.html\
  mekaniker-ordbok-se.pdf mekaniker-ordbok-se.epub mekaniker-ordbok-se.html\
  mekaniker-ordbok-en.pdf mekaniker-ordbok-en.epub mekaniker-ordbok-en.html

all: $(GENERATED)

# Make sure new enough version of dblatex with 'se' support is used
checkdblatex:
	dpkg --compare-versions 0.3.12 le $$(dblatex --version | awk '{print $$3}')


clean:
	$(RM) *~

distclean: clean
	$(RM) glossary.xml $(GENERATED)

XMLLINTOPTS = --nonet --noout  --xinclude --postvalid
lint: book.xml glossary-nb.xml
	xmllint $(XMLLINTOPTS) book.xml

epubcheck: book.epub
	epubcheck book.epub

check: lint epubcheck

glossary-nb.xml: make-glossary meksme-utf8.xml
	LC_COLLATE=nb_NO.UTF-8 ./make-glossary --output $@ nb

glossary-se.xml: make-glossary meksme-utf8.xml
	LC_COLLATE=nb_NO.UTF-8 ./make-glossary --output $@ se

glossary-en.xml: make-glossary meksme-utf8.xml
	LC_COLLATE=nb_NO.UTF-8 ./make-glossary --output $@ en

DBLATEX_OPTS = \
	-b xetex \
	--indexstyle=myindexstyle.ist \
	-V \
	-P latex.index.tool=xindy \
	--param=latex.index.language=norwegian \
	-p pdf.xsl

mekaniker-ordbok-nb.pdf: $(SOURCE_NB)
	dblatex $(DBLATEX_OPTS) -o $@ book.xml

mekaniker-ordbok-nb.epub: $(SOURCE_NB)
	dbtoepub -s epub.xsl book.xml -o $@

mekaniker-ordbok-nb.html: $(SOURCE_NB)
	xmlto -m pdf.xsl html-nochunks book.xml
	mv book.html $@

book-se.xml: book.xml
	sed -e 's/-nb/-se/' -e 's/"nb"/"se"/' < $^ >$@

mekaniker-ordbok-se.pdf: checkdblatex $(SOURCE_SE)
	dblatex $(DBLATEX_OPTS) -o $@ book-se.xml

mekaniker-ordbok-se.epub: $(SOURCE_SE)
	dbtoepub -s epub.xsl book-se.xml -o $@

mekaniker-ordbok-se.html: $(SOURCE_SE)
	xmlto -m pdf.xsl html-nochunks book-se.xml
	mv book-se.html $@

book-en.xml: book.xml
	sed -e 's/-nb/-en/' -e 's/"nb"/"en"/' < $^ >$@

mekaniker-ordbok-en.pdf: $(SOURCE_EN)
	dblatex $(DBLATEX_OPTS) -o $@ book-en.xml

mekaniker-ordbok-en.epub: $(SOURCE_EN)
	dbtoepub -s epub.xsl book-en.xml -o $@

mekaniker-ordbok-en.html: $(SOURCE_EN)
	xmlto -m pdf.xsl html-nochunks book-en.xml
	mv book-en.html $@
