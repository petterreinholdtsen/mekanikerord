Ordbok for mekanikeren
======================

Oppdatert utgave av disse filene kan hentes fra
[gitlab](https://gitlab.com/petterreinholdtsen/mekanikerord).

Når en ser på lærerike og morsomme Youtube-filmer om metalldreing,
fresing, boring, sliping og pussing, som de fra for eksempel
[mrpete222](https://yewtu.be/channel/UCKLIIdKEpjAnn8E76KP7sQg) og
[This Old Tony](https://yewtu.be/channel/UC5NO8MgTQKHAWXp6z8Xl7yQ), så
lærer en seg de engelske begrepene ganske raskt.  Men hva heter de på
norsk?  Behovet for norske begreper ble starten på dette prosjektet.
Jeg fant ingen god nettbasert oversikt.  Det jeg fant var
grunnlagsdata, så jeg endte opp med å lage oversikten selv.  Når jeg
først var i gang laget jeg like godt en bok.  Og en bok ble til flere,
med fokus på ulike språk. :)

Boken er basert på grunnlagsdata samlet under ledelse av Svein Lund.
Datasettet ble brukt til å lage boken "Mekanihkkársánit : Mekanikerord
= Mekaanisen alan sanasto = Mechanic's words" utgitt av [Samisk
utdanningsråd i 1999](https://ovttas.no/nb/girji_mekanikerord), (ISBN
82-7954-042-3), som så vidt jeg vet er utsolgt fra forlaget.

Jeg startet med [en
Filemaker-XML-fil](https://gtsvn.uit.no/biggies/trunk/termdb/src/db-colls/terms/mekanikk-1999/meksme-utf8.xml)
donert av Svein Lund til Universitetet i Tromsø.  Den samme filen ble
donert til meg, Petter Reinholdtsen, av Svein Lund og Sametinget, for
å kunne lage denne Creative Commons-lisensierte oversikten på web og i
bokform.

Filemaker-filen lå i Subversion-depotet tilgjengelig fra
https://gtsvn.uit.no/biggies/trunk/termdb/, som ser ut til å ha vært
brukt av risten.no, nå omdirigert til [en samisk ordbok på
web](http://sátni.org/).  Jeg fant en tidligere webutgave av samme
datasettet på http://www.girji.info/ruovdi/meksanit.htm .

En historie om 1999-boken finnes på
http://www.skuvla.info/skolehist/sveinl-n.htm .

En kilde til norsk språk om maskinering er
http://www.tsbfengineering.com/jorgen .
