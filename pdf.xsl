<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

  <!-- xsl:param name="paper.type">a4</xsl:param -->
  <xsl:param name="double.sided">1</xsl:param>
  <xsl:param name="page.width">6in</xsl:param>
  <xsl:param name="page.height">9in</xsl:param>
  <xsl:param name="page.margin.inner">0.75in</xsl:param>
  <xsl:param name="page.margin.outer">0.50in</xsl:param>
  <xsl:param name="page.margin.top">0.55in</xsl:param>
  <xsl:param name="page.margin.bottom">0.55in</xsl:param>
  <xsl:param name="latex.class.book">book</xsl:param>
  <xsl:param name="latex.class.options">a4paper,openright,twoside</xsl:param>

  <!-- The TOC links in the titles, and in blue. -->
  <!-- ensure URLs in the text do not end up light gray too -->
  <xsl:param name="latex.hyperparam">linktocpage,colorlinks,linkcolor=black,urlcolor=black,pdfstartview=FitH</xsl:param>

  <!-- No use showing table of content, it is empty -->
  <xsl:param name="doc.toc.show">0</xsl:param>

  <!-- no need for a separate author list -->
  <xsl:param name="doc.collab.show">0</xsl:param>

  <!-- disable revision history until there is a history to display -->
  <xsl:param name="latex.output.revhistory">0</xsl:param>

  <!-- enable draft mode until ready to publish -->
  <xsl:param name="draft.mode">no</xsl:param>
  <xsl:param name="draft.watermark">0</xsl:param>

  <!-- insert a few latex tricks at the top of the .tex document -->
  <xsl:param name="latex.begindocument">
    <xsl:text>% start of latex.begindocument
\usepackage{multicol}

% unboxed = wrap glossary term
% multicols = get two columns only in mainpart
\setlist[description]{%
  style=unboxed,
  before*=\begin{multicols}{2},
  after*=\end{multicols}
%  first*=
%  topsep=30pt,               % space before start / after end of list
%  itemsep=5pt,               % space between items
%  font=\normalfont,
}

% Trick to avoid many words sticking out of the right margin of the text.
\sloppy

% The microtype package provides the ability to micromanage your
% typography. When invoked without any options it does some nice things
% like protruding punctuation over the edge of the right margin to make
% the margin appear smoother. Basically it makes your book look more
% professional with very little effort. It also has a ton of options if
% you want to micromanage even more.
\usepackage{microtype}

% Disable headers and footer texts
\pagestyle{myheadings}

\def\DBKpublisher{Petter Reinholdtsen \\ Oslo}

% Cludge to replace DBKcover \def in
% /usr/share/texmf/tex/latex/dblatex/style/dbk_title.sty where author
% and publisher is missing
\def\DBKcover{
\ifthenelse{\equal{\DBKedition}{}}{\def\edhead{}}{\def\edhead{Ed. \DBKedition}}
% interligne double
\setlength{\oldbaselineskip}{\baselineskip}
\setlength{\baselineskip}{2\oldbaselineskip}
\textsf{
\vfill
\vspace{2.5cm}
\begin{center}
  \huge{\textbf{\DBKtitle}}\\ %
  \ifx\DBKsubtitle\relax\else%
    \underline{\ \ \ \ \ \ \ \ \ \ \ }\\ %
    \ \\ %
    \huge{\textbf{\DBKsubtitle}}\\ %
    \fi
  \  \\ %
%  \huge{\editor} \\%
\end{center}
\vfill
\setlength{\baselineskip}{\oldbaselineskip}
\hspace{1cm}
\vspace{1cm}
\begin{center}
\Large{\DBKpublisher} \\
\end{center}
}
%
\newcommand{\sectionline}{%
  \nointerlineskip \vspace{\baselineskip}%
  \hspace{\fill}\rule{0.5\linewidth}{.7pt}\hspace{\fill}%
  \par\nointerlineskip \vspace{\baselineskip}%
  }%
%
% Format for the other pages
\newpage
\setlength{\baselineskip}{\oldbaselineskip}
\lfoot[]{}
}

\begin{document}
% end of latex.begindocument
    </xsl:text>
  </xsl:param>

  <!-- disable chapter numbering for glossary -->
  <xsl:param name="glossary.numbered">0</xsl:param>

  <!-- disable chapter numbering for index -->
  <xsl:param name="index.numbered">0</xsl:param>

  <xsl:param name="local.l10n.xml" select="document('')"/>
  <l:i18n xmlns:l="http://docbook.sourceforge.net/xmlns/l10n/1.0">
    <l:l10n language="nb">
      <!-- Fix bugs in default nb locale -->
      <l:dingbat key="startquote" text="«"/>
      <l:dingbat key="endquote" text="»"/>
      <l:dingbat key="nestedstartquote" text="‘"/>
      <l:dingbat key="nestedendquote" text="’"/>
      <l:gentext key="Copyright" text=""/>
    </l:l10n>
    <l:l10n language="nn">
      <!-- Fix bugs in default nn locale -->
      <l:dingbat key="startquote" text="«"/>
      <l:dingbat key="endquote" text="»"/>
      <l:dingbat key="nestedstartquote" text="‘"/>
      <l:dingbat key="nestedendquote" text="’"/>

  <!--
Workaround for missing 'se' support in dblatex/docbook, use 'nn' as
replacement and rewrite the strings we use.  Note PDF use some of
these, while HTML and ePub uses others.

A more future proof approach that would help others would be to submit
a se.xsl file to
https://github.com/docbook/xslt10-stylesheets/tree/master/gentext/locale .
  -->
      <l:gentext key="Editedby" text="Redigert av (se)"/>
      <l:gentext key="Abstract" text="Čoahkkáigeassu"/>
      <l:gentext key="TableofContents" text="Sisdoallu"/>
      <l:gentext key="Copyright" text=""/>
      <l:gentext key="Glossary" text="Sátnelistu"/>
      <l:gentext key="glossary" text="Sátnelistu"/>
      <l:gentext key="glosssee" text="Geahča"/>
      <l:gentext key="GlossSee" text="Geahča"/>
      <l:context name="glossary">
	<l:template name="see" text="Geahča «%t»."/>
      </l:context>
    </l:l10n>
  </l:i18n>

<!--
Make final page blank, which is required for PDFs inteneded for
extended distribution with LuLu.
-->

<xsl:param name="latex.enddocument">
  <xsl:text>\pagebreak
\thispagestyle{empty}
~
\end{document}
  </xsl:text>
</xsl:param>

</xsl:stylesheet>
